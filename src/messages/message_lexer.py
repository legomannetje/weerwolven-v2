# Generated from D:/Code/lykos/src/messages\message_lexer.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\23")
        buf.write("\u00eb\b\1\b\1\b\1\b\1\b\1\b\1\b\1\4\2\t\2\4\3\t\3\4\4")
        buf.write("\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4")
        buf.write("\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20")
        buf.write("\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26")
        buf.write("\t\26\4\27\t\27\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\2\3\2\3\2\6\2P\n\2\r\2\16\2Q\3\3\3\3\3\3\3\3")
        buf.write("\3\4\3\4\3\4\3\4\3\5\6\5]\n\5\r\5\16\5^\3\6\3\6\3\7\3")
        buf.write("\7\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\t\3\t\6\tu\n\t\r\t\16\tv\3\n\3\n\3\n\3\n\3\n\3")
        buf.write("\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3")
        buf.write("\f\3\f\3\f\6\f\u008d\n\f\r\f\16\f\u008e\3\r\3\r\3\r\3")
        buf.write("\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3")
        buf.write("\20\3\20\3\20\3\21\6\21\u00a3\n\21\r\21\16\21\u00a4\3")
        buf.write("\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\24\6\24\u00bf\n\24\r\24\16\24\u00c0\3\25\3\25")
        buf.write("\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\6\31\u00df\n\31\r\31\16\31\u00e0")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\2\2\34\t")
        buf.write("\3\13\4\r\5\17\6\21\7\23\b\25\t\27\n\31\2\33\2\35\13\37")
        buf.write("\f!\r#\2%\16\'\17)\2+\2-\20/\2\61\2\63\21\65\2\67\229")
        buf.write("\2;\23\t\2\3\4\5\6\7\b\7\6\2]]__}}\177\177\4\2C\\c|\6")
        buf.write("\2##<<}}\177\177\6\2*+<<}}\177\177\5\2*+}}\177\177\2\u00f8")
        buf.write("\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\3\17\3\2\2\2\3\21")
        buf.write("\3\2\2\2\3\23\3\2\2\2\3\25\3\2\2\2\4\27\3\2\2\2\4\31\3")
        buf.write("\2\2\2\4\33\3\2\2\2\5\35\3\2\2\2\5\37\3\2\2\2\5!\3\2\2")
        buf.write("\2\5#\3\2\2\2\5%\3\2\2\2\6\'\3\2\2\2\6)\3\2\2\2\6+\3\2")
        buf.write("\2\2\7-\3\2\2\2\7/\3\2\2\2\7\61\3\2\2\2\7\63\3\2\2\2\7")
        buf.write("\65\3\2\2\2\b\67\3\2\2\2\b9\3\2\2\2\b;\3\2\2\2\tO\3\2")
        buf.write("\2\2\13S\3\2\2\2\rW\3\2\2\2\17\\\3\2\2\2\21`\3\2\2\2\23")
        buf.write("b\3\2\2\2\25f\3\2\2\2\27t\3\2\2\2\31x\3\2\2\2\33}\3\2")
        buf.write("\2\2\35\u008c\3\2\2\2\37\u0090\3\2\2\2!\u0094\3\2\2\2")
        buf.write("#\u0098\3\2\2\2%\u009d\3\2\2\2\'\u00a2\3\2\2\2)\u00a6")
        buf.write("\3\2\2\2+\u00ab\3\2\2\2-\u00be\3\2\2\2/\u00c2\3\2\2\2")
        buf.write("\61\u00c6\3\2\2\2\63\u00cb\3\2\2\2\65\u00cf\3\2\2\2\67")
        buf.write("\u00de\3\2\2\29\u00e2\3\2\2\2;\u00e7\3\2\2\2=>\n\2\2\2")
        buf.write(">P\b\2\2\2?@\7}\2\2@A\7}\2\2AB\3\2\2\2BP\b\2\3\2CD\7\177")
        buf.write("\2\2DE\7\177\2\2EF\3\2\2\2FP\b\2\4\2GH\7]\2\2HI\7]\2\2")
        buf.write("IJ\3\2\2\2JP\b\2\5\2KL\7_\2\2LM\7_\2\2MN\3\2\2\2NP\b\2")
        buf.write("\6\2O=\3\2\2\2O?\3\2\2\2OC\3\2\2\2OG\3\2\2\2OK\3\2\2\2")
        buf.write("PQ\3\2\2\2QO\3\2\2\2QR\3\2\2\2R\n\3\2\2\2ST\7]\2\2TU\3")
        buf.write("\2\2\2UV\b\3\7\2V\f\3\2\2\2WX\7}\2\2XY\3\2\2\2YZ\b\4\b")
        buf.write("\2Z\16\3\2\2\2[]\t\3\2\2\\[\3\2\2\2]^\3\2\2\2^\\\3\2\2")
        buf.write("\2^_\3\2\2\2_\20\3\2\2\2`a\7\61\2\2a\22\3\2\2\2bc\7?\2")
        buf.write("\2cd\3\2\2\2de\b\7\t\2e\24\3\2\2\2fg\7_\2\2gh\3\2\2\2")
        buf.write("hi\b\b\n\2i\26\3\2\2\2jk\n\2\2\2ku\b\t\13\2lm\7}\2\2m")
        buf.write("n\7}\2\2no\3\2\2\2ou\b\t\f\2pq\7\177\2\2qr\7\177\2\2r")
        buf.write("s\3\2\2\2su\b\t\r\2tj\3\2\2\2tl\3\2\2\2tp\3\2\2\2uv\3")
        buf.write("\2\2\2vt\3\2\2\2vw\3\2\2\2w\30\3\2\2\2xy\7}\2\2yz\3\2")
        buf.write("\2\2z{\b\n\b\2{|\b\n\16\2|\32\3\2\2\2}~\7_\2\2~\177\3")
        buf.write("\2\2\2\177\u0080\b\13\n\2\u0080\u0081\b\13\17\2\u0081")
        buf.write("\34\3\2\2\2\u0082\u0083\n\4\2\2\u0083\u008d\b\f\20\2\u0084")
        buf.write("\u0085\7}\2\2\u0085\u0086\7}\2\2\u0086\u0087\3\2\2\2\u0087")
        buf.write("\u008d\b\f\21\2\u0088\u0089\7\177\2\2\u0089\u008a\7\177")
        buf.write("\2\2\u008a\u008b\3\2\2\2\u008b\u008d\b\f\22\2\u008c\u0082")
        buf.write("\3\2\2\2\u008c\u0084\3\2\2\2\u008c\u0088\3\2\2\2\u008d")
        buf.write("\u008e\3\2\2\2\u008e\u008c\3\2\2\2\u008e\u008f\3\2\2\2")
        buf.write("\u008f\36\3\2\2\2\u0090\u0091\7#\2\2\u0091\u0092\3\2\2")
        buf.write("\2\u0092\u0093\b\r\23\2\u0093 \3\2\2\2\u0094\u0095\7<")
        buf.write("\2\2\u0095\u0096\3\2\2\2\u0096\u0097\b\16\24\2\u0097\"")
        buf.write("\3\2\2\2\u0098\u0099\7}\2\2\u0099\u009a\3\2\2\2\u009a")
        buf.write("\u009b\b\17\b\2\u009b\u009c\b\17\16\2\u009c$\3\2\2\2\u009d")
        buf.write("\u009e\7\177\2\2\u009e\u009f\3\2\2\2\u009f\u00a0\b\20")
        buf.write("\n\2\u00a0&\3\2\2\2\u00a1\u00a3\t\3\2\2\u00a2\u00a1\3")
        buf.write("\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5")
        buf.write("\3\2\2\2\u00a5(\3\2\2\2\u00a6\u00a7\7<\2\2\u00a7\u00a8")
        buf.write("\3\2\2\2\u00a8\u00a9\b\22\24\2\u00a9\u00aa\b\22\25\2\u00aa")
        buf.write("*\3\2\2\2\u00ab\u00ac\7\177\2\2\u00ac\u00ad\3\2\2\2\u00ad")
        buf.write("\u00ae\b\23\n\2\u00ae\u00af\b\23\26\2\u00af,\3\2\2\2\u00b0")
        buf.write("\u00b1\n\5\2\2\u00b1\u00bf\b\24\27\2\u00b2\u00b3\7<\2")
        buf.write("\2\u00b3\u00b4\7<\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00bf")
        buf.write("\b\24\30\2\u00b6\u00b7\7}\2\2\u00b7\u00b8\7}\2\2\u00b8")
        buf.write("\u00b9\3\2\2\2\u00b9\u00bf\b\24\31\2\u00ba\u00bb\7\177")
        buf.write("\2\2\u00bb\u00bc\7\177\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00bf")
        buf.write("\b\24\32\2\u00be\u00b0\3\2\2\2\u00be\u00b2\3\2\2\2\u00be")
        buf.write("\u00b6\3\2\2\2\u00be\u00ba\3\2\2\2\u00bf\u00c0\3\2\2\2")
        buf.write("\u00c0\u00be\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1.\3\2\2")
        buf.write("\2\u00c2\u00c3\7<\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c5")
        buf.write("\b\25\25\2\u00c5\60\3\2\2\2\u00c6\u00c7\7}\2\2\u00c7\u00c8")
        buf.write("\3\2\2\2\u00c8\u00c9\b\26\b\2\u00c9\u00ca\b\26\16\2\u00ca")
        buf.write("\62\3\2\2\2\u00cb\u00cc\7*\2\2\u00cc\u00cd\3\2\2\2\u00cd")
        buf.write("\u00ce\b\27\33\2\u00ce\64\3\2\2\2\u00cf\u00d0\7\177\2")
        buf.write("\2\u00d0\u00d1\3\2\2\2\u00d1\u00d2\b\30\n\2\u00d2\u00d3")
        buf.write("\b\30\26\2\u00d3\66\3\2\2\2\u00d4\u00d5\n\6\2\2\u00d5")
        buf.write("\u00df\b\31\34\2\u00d6\u00d7\7}\2\2\u00d7\u00d8\7}\2\2")
        buf.write("\u00d8\u00d9\3\2\2\2\u00d9\u00df\b\31\35\2\u00da\u00db")
        buf.write("\7\177\2\2\u00db\u00dc\7\177\2\2\u00dc\u00dd\3\2\2\2\u00dd")
        buf.write("\u00df\b\31\36\2\u00de\u00d4\3\2\2\2\u00de\u00d6\3\2\2")
        buf.write("\2\u00de\u00da\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0\u00de")
        buf.write("\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e18\3\2\2\2\u00e2\u00e3")
        buf.write("\7}\2\2\u00e3\u00e4\3\2\2\2\u00e4\u00e5\b\32\b\2\u00e5")
        buf.write("\u00e6\b\32\16\2\u00e6:\3\2\2\2\u00e7\u00e8\7+\2\2\u00e8")
        buf.write("\u00e9\3\2\2\2\u00e9\u00ea\b\33\n\2\u00ea<\3\2\2\2\25")
        buf.write("\2\3\4\5\6\7\bOQ^tv\u008c\u008e\u00a4\u00be\u00c0\u00de")
        buf.write("\u00e0\37\3\2\2\3\2\3\3\2\4\3\2\5\3\2\6\7\3\2\7\5\2\4")
        buf.write("\4\2\6\2\2\3\t\7\3\t\b\3\t\t\t\5\2\t\t\2\3\f\n\3\f\13")
        buf.write("\3\f\f\4\6\2\4\7\2\t\r\2\t\16\2\3\24\r\3\24\16\3\24\17")
        buf.write("\3\24\20\7\b\2\3\31\21\3\31\22\3\31\23")
        return buf.getvalue()


class message_lexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    IN_TAG = 1
    IN_TAG_PARAM = 2
    IN_SUB = 3
    IN_CONV = 4
    IN_SPEC = 5
    IN_ARGLIST = 6

    TEXT = 1
    OPEN_TAG = 2
    OPEN_SUB = 3
    TAG_NAME = 4
    TAG_SLASH = 5
    TAG_SEP = 6
    CLOSE_TAG = 7
    TAG_PARAM = 8
    SUB_FIELD = 9
    SUB_CONVERT = 10
    SUB_SPEC = 11
    CLOSE_SUB = 12
    SUB_IDENTIFIER = 13
    SPEC_VALUE = 14
    OPEN_ARGLIST = 15
    ARGLIST_VALUE = 16
    CLOSE_ARGLIST = 17

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE", "IN_TAG", "IN_TAG_PARAM", "IN_SUB", "IN_CONV", 
                  "IN_SPEC", "IN_ARGLIST" ]

    literalNames = [ "<INVALID>",
            "'['", "'{'", "'/'", "'='", "'!'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>",
            "TEXT", "OPEN_TAG", "OPEN_SUB", "TAG_NAME", "TAG_SLASH", "TAG_SEP", 
            "CLOSE_TAG", "TAG_PARAM", "SUB_FIELD", "SUB_CONVERT", "SUB_SPEC", 
            "CLOSE_SUB", "SUB_IDENTIFIER", "SPEC_VALUE", "OPEN_ARGLIST", 
            "ARGLIST_VALUE", "CLOSE_ARGLIST" ]

    ruleNames = [ "TEXT", "OPEN_TAG", "OPEN_SUB", "TAG_NAME", "TAG_SLASH", 
                  "TAG_SEP", "CLOSE_TAG", "TAG_PARAM", "TAG_OPEN_SUB", "TAG_PARAM_CLOSE", 
                  "SUB_FIELD", "SUB_CONVERT", "SUB_SPEC", "SUB_OPEN_SUB", 
                  "CLOSE_SUB", "SUB_IDENTIFIER", "CONV_SPEC", "CLOSE_CONV", 
                  "SPEC_VALUE", "SPEC_SEP", "SPEC_OPEN_SUB", "OPEN_ARGLIST", 
                  "CLOSE_SPEC", "ARGLIST_VALUE", "ARGLIST_OPEN_SUB", "CLOSE_ARGLIST" ]

    grammarFileName = "message_lexer.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[0] = self.TEXT_action 
            actions[7] = self.TAG_PARAM_action 
            actions[10] = self.SUB_FIELD_action 
            actions[18] = self.SPEC_VALUE_action 
            actions[23] = self.ARGLIST_VALUE_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def TEXT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
             self.append_text() 
     

        if actionIndex == 1:
             self.append_text("{") 
     

        if actionIndex == 2:
             self.append_text("}") 
     

        if actionIndex == 3:
             self.append_text("[") 
     

        if actionIndex == 4:
             self.append_text("]") 
     

    def TAG_PARAM_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 5:
             self.append_text() 
     

        if actionIndex == 6:
             self.append_text("{") 
     

        if actionIndex == 7:
             self.append_text("}") 
     

    def SUB_FIELD_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 8:
             self.append_text() 
     

        if actionIndex == 9:
             self.append_text("{") 
     

        if actionIndex == 10:
             self.append_text("}") 
     

    def SPEC_VALUE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 11:
             self.append_text() 
     

        if actionIndex == 12:
             self.append_text(":") 
     

        if actionIndex == 13:
             self.append_text("{") 
     

        if actionIndex == 14:
             self.append_text("}") 
     

    def ARGLIST_VALUE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 15:
             self.append_text() 
     

        if actionIndex == 16:
             self.append_text("{") 
     

        if actionIndex == 17:
             self.append_text("}") 
     


